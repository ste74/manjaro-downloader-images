#!/bin/env bash

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

clear

main=/usr/bin/manjaro-downloader-images
tmpdir=$HOME/.tmp-iso-folder
trap 'rm -Rf $tmpdir; exit' ERR EXIT

#Main Window settings
gui() {
    local icon=/usr/share/icons/manjaro/maia/maia.svg
    local window_icon=/usr/share/icons/manjaro/maia/32x32.png
    local up=5
    local larg=600
    
    if [ -r "$icon" ] ; then
        GDK_BACKEND=x11 /usr/bin/yad --center --window-icon="$icon" --width=$larg --height=$up --image="$window_icon" --title="Manjaro Developer Images" "$@"
    else
        GDK_BACKEND=x11 /usr/bin/yad --center --width=$larg --height=$up --title="Manjaro Developer Images" "$@"
    fi
}

Welcome() 
{  DE=$(gui --text-align=center --text="<b>Manjaro Dev Image Downloader</b>\nA simple <b>GUI</b> to download the lastest developer images." \
    --form --separator="" \
    --field="Select Edition"::CB "XFCE!Plasma!Plasma-daily!Gnome")
}

Version() {
    VER=$(gui --text-align=center --form --separator="" --width=300 --button="Ok" \
    --field="Select Version"::CB "Minimal!Full")
}

get_xfce() {
    img=xfce
    Version
    case $VER in
        Minimal) imgs=mini ;;
        Full)    imgs=full
    esac 

    if [[ $imgs == 'mini' ]]; then
        if [[ -d $HOME/Manjaro-$img-Iso ]]; then
            checks=$(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name | test (\"zip\")) | .browser_download_url " | grep 'minimal')
            check=$(basename $checks) 
            if [[ -e $HOME/Manjaro-$img-Iso/$check ]]; then
                gui --text-aligned=center --text='You have already the last ISO' --button='Change version':0 --button='Exit':1
                if [[ $? -eq 0 ]]; then
                    exec $main
                else exit 1
                fi
            fi
        fi
        wget -nc -P $tmpdir $(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name | test (\"minimal\")) | .browser_download_url ") 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Downloading at \2\/s, ETA \3/' | gui --progress --pulsate --title="Downloading File..." --auto-close --no-buttons
    else
        if [[ -d $HOME/Manjaro-$img-Iso ]]; then
            checks=$(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name | test (\"zip\")) | .browser_download_url " | sed '/minimal/d')
            check=$(basename $checks) 
            if [[ -e $HOME/Manjaro-$img-Iso/$check ]]; then
                gui --text-aligned=center --text='You have already the last ISO' --button='Change version':0 --button='Exit':1
                if [[ $? -eq 0 ]]; then
                    exec $main
                else exit 1
                fi            fi
        wget -nc -P $tmpdir $(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name) | .browser_download_url " | sed '/minimal/d') 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Downloading at \2\/s, ETA \3/' | gui --progress --pulsate --title="Downloading File..." --auto-close --no-buttons
        fi
    fi

}

get_plasma() {
    img=plasma
    Version
    case $VER in
        Minimal) imgs=mini ;;
        Full)    imgs=full
    esac 

    if [[ $imgs == 'mini' ]]; then
        if [[ -d $HOME/Manjaro-$img-Iso ]]; then
            checks=$(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name | test (\"zip\")) | .browser_download_url " | grep 'minimal')
            check=$(basename $checks) 
            if [[ -e $HOME/Manjaro-$img-Iso/$check ]]; then
                gui --text-aligned=center --text='You have already the last ISO' --button='Change version':0 --button='Exit':1
                if [[ $? -eq 0 ]]; then
                    exec $main
                else exit 1
                fi
            fi
        fi
        wget -nc -P $tmpdir $(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name | test (\"minimal\")) | .browser_download_url ") 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Downloading at \2\/s, ETA \3/' | gui --progress --pulsate --title="Downloading File..." --auto-close --no-buttons
    else
        if [[ -d $HOME/Manjaro-$img-Iso ]]; then
            checks=$(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name | test (\"zip\")) | .browser_download_url " | sed '/minimal/d')
            check=$(basename $checks) 
            if [[ -e $HOME/Manjaro-$img-Iso/$check ]]; then
                gui --text-aligned=center --text='You have already the last ISO' --button='Change version':0 --button='Exit':1
                if [[ $? -eq 0 ]]; then
                    exec $main
                else exit 1
                fi            fi
        wget -nc -P $tmpdir $(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name) | .browser_download_url " | sed '/minimal/d') 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Downloading at \2\/s, ETA \3/' | gui --progress --pulsate --title="Downloading File..." --auto-close --no-buttons
        fi
    fi

}

get_plasma-daily() {
    img=plasma-daily 
    if [[ -d $HOME/Manjaro-$img-Iso ]]; then
        checks=$(curl -s https://api.github.com/repos/manjaro/plasma-daily/releases/latest | jq -r ".assets[] | select(.name | test (\"zip\")) | .browser_download_url ")
        check=$(basename $checks) 
        if [[ -e $HOME/Manjaro-$img-Iso/$check ]]; then
            gui --text-aligned=center --text='You have already the last ISO' --button='Change version':0 --button='Exit':1
            if [[ $? -eq 0 ]]; then
                exec $main
            else exit 1
            fi
        fi
    fi
    wget -nc -P $tmpdir $( curl -s https://api.github.com/repos/manjaro/plasma-daily/releases/latest | jq -r ".assets[] | select(.name) | .browser_download_url ") 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Downloading at \2\/s, ETA \3/' | gui --progress --pulsate --title="Downloading File..." --auto-close --no-buttons
}

get_gnome() {
    img=gnome     
	if [[ -d $HOME/Manjaro-$img-Iso ]]; then
        checks=$(curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name | test (\"zip\")) | .browser_download_url ")
        check=$(basename $checks) 
        if [[ -e $HOME/Manjaro-$img-Iso/$check ]]; then
            gui --text-aligned=center --text='You have already the last ISO' --button='Change version':0 --button='Exit':1
            if [[ $? -eq 0 ]]; then
                exec $main
            else exit 1
            fi
        fi
    fi
    wget -nc -P $tmpdir $( curl -s https://api.github.com/repos/manjaro-${img}/download/releases/latest | jq -r ".assets[] | select(.name) | .browser_download_url ") 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Downloading at \2\/s, ETA \3/' | gui --progress --pulsate --title="Downloading File..." --auto-close --no-buttons
}

_unzip() {
    cd $tmpdir
    7z t *.zip &> /dev/null &
    while pkill -0 7z; do
    	echo 'Checking'
    done | gui --progress --pulsate --no-buttons --progress-text='Checking...' --auto-close
    if [[ $i -eq 0 ]]; then
        7z x -y *.zip &> /dev/null & 
        while pkill -0 7z; do
            echo 'Unzipping... '
        done | gui --progress --pulsate --no-buttons --progress-text='Unzipping...' --auto-close
    else gui --text-aligned=center --text='Iso corrupted: choice again an Iso' --button='Change version':0 --button='Exit':1
        if [[ $? -eq 0 ]]; then
            exec $main
        else exit 1
        fi
    fi
}

final() {
    if [[ ! -d $HOME/Manjaro-$img-Iso ]]; then
        mkdir $HOME/Manjaro-$img-Iso
    fi
    cd $tmpdir
    mv *.iso *.sha256 *.txt $HOME/Manjaro-$img-Iso
}

check_connection() {
    wget -q --tries=10 --timeout=20 --spider https://google.com
    if [[ $? != 0 ]]; then
        gui --text-align=center --text="Check your internet connection \nyou are offline" --button="Ok":0 --button="Exit":1
        case $? in
            0) exec $main ;;
            1) exit 1
        esac
    fi
}

final_msg() {
    gui --text="Done, the Iso can be found at $HOME/Manjaro-$img-Iso " --button="Exit":0 --button="Open folder":1 --button="New Iso":2
        case $? in
            0) exit 1 ;;
            1) xdg-open $HOME/Manjaro-$img-Iso ;;
            2) exec $main
        esac
}

#Main program

check_connection

if [[ ! -d $tmpdir ]]; then
	mkdir $tmpdir
fi

Welcome
    case $DE in
        XFCE) get_xfce ;;
        Plasma) get_plasma;;
        Plasma-daily) get_plasma-daily;;
        Gnome) get_gnome
    esac

_unzip
final
final_msg

#Done
